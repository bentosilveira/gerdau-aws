### Configuraçoes do Gitlab

para executar a pipeline corretamente, é necessário crias as seguintes
variáveis no Gitlab:

- REGISTRY_URL: Deve conter o endereço completo do registry, sem o protocolo.
  Ou seja, não deve ter "http://" ou "https://" no começo. Ex: `registry.yaman.com.br:5000/registry`

- REGISTRY_USER: O usuário para login no registry.

- REGISTRY_PASSWORD: A senha do usuário. Recomenda-se marcar esta variável como "masked"

Sem essas variáveis, a pipeline encerrará com erro.

### Configurações do AWS

Para facilitar o uso do comando `aws`, é possível embutir arquivos com
credenciais e configurações no container.

As credenciais podem ser informadas editando o arquivo `credentials` e
as configurações editando o arquivo `config`, diretamente no projeto.
Esta não é a melhor maneira, já que deixa as credenciais expostas.

A melhor maneira é criar variáveis do tipo `file` na configuração de
CI/CD do projeto. As variáveis são:

- AWS_CREDENTIALS: Contém as credenciais. Exemplo de conteúdo desta variável:

```
[default]
aws_access_key_id="sua chave aqui"
aws_secret_access_key="seu segredo aqui"
```

- AWS_CONFIG: Contém configurações personalizadas. Os possíveis valores podem
  ser vistos na página do projeto em https://github.com/aws/aws-cli/blob/v2/README.rst .
  Exemplo:

```
[default]
# Optional, to define default region for this profile.
region=us-west-1
```
