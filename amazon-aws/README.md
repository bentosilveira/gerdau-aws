### Executando o container

Use o comando:

    docker run --rm -it amazon/aws-cli <sub-comando>

Mais informações no [Docker Hub](https://github.com/aws/aws-cli/blob/v2/README.rst)
da imagem oficial provida pela Amazon.

### Diferenças em relação a oficial

Esta imagem inclui um arquivo de configuração e credenciais direto na imagem.
